import Mock from "mockjs";

Mock.mock("/mock/bottom", {
  code: 200,
  "data|1-4": [
    {
      date: "@date('yyyy-MM-dd')",
      name: "@cname()",
      address: "@county(true)",
    },
  ],
});
