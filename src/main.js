import Vue from "vue";
import App from "./App.vue";

import store from "./store";

import "@/mock/mockServer";

import { Button, Input, Select, Table, TableColumn } from "element-ui";

// Vue.use(Button);
Vue.component("el-button", Button);
Vue.component("el-input", Input);
Vue.component("el-select", Select);
Vue.component("el-table", Table);
Vue.component("el-table-column", TableColumn);

Vue.config.productionTip = false;

new Vue({
  store,
  render: (h) => h(App),
}).$mount("#app");
