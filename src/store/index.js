import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    show: true,
  },

  mutations: {
    switchUser(state, userName) {
      if (userName == "admin") {
        state.show = true;
      } else if (userName == "editor") {
        state.show = false;
      }
    },
  },
});

export default store;
